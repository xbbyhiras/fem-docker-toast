# Creating a network

To list the networks already set up:
```sh
docker network ls
```

Creating a bridge allow all docker images connected to it to talk to each other.
## Creating the bridge
```sh
docker network create --driver=bridge app-bridge
```

## Connecting to the bridge

First  container hold the `mongo` db
```sh
docker run -d --network=app-bridge -p 27017:27017 --name=db --rm mongo
```

Second container is the `mongo` client. The name of the first container `db` is used as URL for the client container.
```sh
docker run -it --network=app-bridge --rm mongo mongosh --host db
```

```sh
Current Mongosh Log ID: 62f26c6ab55da09a1b85b931
Connecting to:          mongodb://db:27017/?directConnection=true&appName=mongosh+1.5.4
Using MongoDB:          5.0.10
Using Mongosh:          1.5.4
```

## Connecting a node app

The node app should be similar to:
```js
import { MongoClient } from 'mongodb'

const url = process.env.MONGO_CONNECTION_STRING || 'mongodb://localhost:27017'
const dbName = 'dockerApp'
const collectionName = 'count'

const start = async () => {
  const client = await MongoClient.connect(url)
  const db = client.db(dbName)
  const collection = db.collection(collectionName)

```
Create the Dockerfile for the node app, called `networking`
```sh
docker build . -t networking:1.0.0
```

Run it with
```sh
docker run -u "node" -p 3000:3000 --network=app-bridge --env MONGO_CONNECTION_STRING=mongodb://db:27017 networking:1.0.0
```

To test it, visit http://0.0.0.0:3000 and http://0.0.0.0:3000/add

