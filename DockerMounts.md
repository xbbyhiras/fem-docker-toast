# Bind mount

Example of mounting (from a `CRA` application root folder):
```sh
docker run --mount type=bind,source="$(pwd)"/build,target=/usr/share/nginx/html -p 8080:80 nginx
```

The workflow here is to `npm run build`in your local and the image serves whatever is in the build folder.

# Volume mount

```sh
docker run --env DATA_PATH=/data/num.txt --mount type=volume,src=incr-data,target=/data incr
```

# Tmpfs mount

https://wiki.archlinux.org/title/Tmpfs

# Named pipes mount

https://docs.microsoft.com/en-us/windows/win32/ipc/named-pipes