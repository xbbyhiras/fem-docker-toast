import http from 'http'

http.createServer((req, res) => {
    console.log({ received: req })
    res.end('hello', 'utf-8')
}).listen(3000)
console.log('server started', { port: 3000 })
