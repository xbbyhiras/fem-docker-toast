# Build and run the first image

Create a file named `Dockerfile`. Yes, there is no extension.
```dockerfile
FROM node AS pull_node

CMD [ "node", "-e", "console.log('Hello World')" ]
``` 

Build the image with:
```sh
docker build . -t test-cont
```

Run it with:
```sh
docker run test-cont
```

## Tagging rules
The `-t / --tag` param should be used for versionning the containers.
A Possibility is to use the same version number of the `package.json` file the container build is copying.

E.g. Let's say the `package.json` file is 
```json
{
  "name": "fem-docker-toast",
  "version": "1.0.0",
  ...
}
```
The tag would then be `fem-docker-toast:1.0.0`

## CMD Prio rules
Note: Overwritten command do not get executed.
Only the winning command gets to be executed.

### 1. Execute the command from the docker image.
E.g. for the `node:alpine` image, you can inspect and see
```json
        "Cmd": [
            "/bin/sh",
            "-c",
            "#(nop) ",
            "CMD [\"/bin/sh\"]"
        ],
```
It will execute the command
```sh
/bin/sh
```

### 2. Execute the CMD line from the Dockerfile
E.g. in our current `Dockerfile` there is
```Dockerfile
CMD [ "node", "-e", "console.log('Hello World')" ]
```
This will overwrite the `"Cmd:"` field in the image config and execute `"node"` instead.
Multiple `CMD` overwrite themselves and the last one get to be the one executed.

Here it will execute
```sh
node -e "console.log('Hello World')"
```

### 3. Execute the CMD from the command line
If the `docker run` command has a `[COMMAND]` param, this will overwrite all the previous one.
E.g.
```sh
docker run test-cont ls
```
will run the command
```sh
ls
```

## Run with clean up params
An easy way to handle the `stop` and `remove` of running container is to add `--init --rm` to the `docker run` command.
It will handle the `SIGTERM` C+d and will remove the stopped container for you.

```sh
docker run test-cont
```
becomes
```sh
docker run --init --rm test-cont
```

## Opening ports
As much as you could open the ports from the `Dockerfile`, it is more practical to do it from the cli in case you need to modify the ports for the execution of the container.

Supposing the server app in the container is listening on port `3000`, `--publish` will open that port to the host.

```sh
docker run --init --rm  --publish 3000:3000 test-cont
```

# Toast file
The equivalent of the build+run image in `Toast` is:
```yml
image: node 
user: node
location: /

tasks:
  start_server:
    cache: false
    input_paths:
      - index.mjs
    ports:
      - 3000:3000
    command: |
      node index.mjs
```
- Name the file `toast.yml`
- Run with:  `toast`
- Test with: `curl localhost:3000 `
