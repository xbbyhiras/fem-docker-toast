import hapi from '@hapi/hapi'
import pino from 'hapi-pino'

const start = async () => {
  const server = hapi.server({
    host: '0.0.0.0', // host localhost will keep you in the container.
    port: process.env.PORT | 3000
  })

  server.route({
    method: 'GET',
    path: '/',
    handler: () => ({ succes: true })
  })

  // https://github.com/pinojs/hapi-pino/issues/147
  await server.register({
    plugin: pino,
    options: {
      redact: ['req.headers.authorization'],
      transport: {
        target: 'pino-pretty',
        options: {
          colorize: true,
          minimumLevel: 'info',
          levelFirst: true,
          messageFormat: true,
          timestampKey: 'time',
          translateTime: true,
          singleLine: false,
          mkdir: true,
          append: true
        }
      }
    }
  })

  await server.start()

  return server
}

start()
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
