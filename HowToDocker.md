# Docker Course from FEM

## intro

## Containers

### chroot

Type the command to start a docker image of ubuntu
```sh
docker run -it --name docker-host --rm --privileged ubuntu:kinetic
```

With `Toast` you could do a `toast.yml` file
```yml
image: ubuntu
tasks:
  update:
    command: |
      apt-get update
```

and run the command
```sh
toast -s # or --shell
```

The system is empty, to paste a command and chroot it in a specific directory:
* `which <command>` will tell where to copy it from
* `ldd /path/to/<command>` will tell you what are the dependencies to copy as well

Any libraries that do not have a fully qualified path can be ignored from the copy:
```sh
root@c50bd1d881e2:/$ which cat
/usr/bin/cat
root@c50bd1d881e2:/$ ldd /usr/bin/cat
        linux-vdso.so.1 (0x0000ffffbcb8c000)
        libc.so.6 => /lib/aarch64-linux-gnu/libc.so.6 (0x0000ffffbc980000)
        /lib/ld-linux-aarch64.so.1 (0x0000ffffbcb53000)
root@c50bd1d881e2:/$ cp /lib/aarch64-linux-gnu/libc.so.6 /lib/ld-linux-aarch64.so.1 my-new-root/lib/
root@c50bd1d881e2:/$ 
```

### Namespace

Let's use the tool called `debootstrap` for creating a chroot easily.
`kinetic` is the latest ubuntu build at the time: https://hub.docker.com/_/ubuntu/ 
The `toast` file becomes
```yaml
image: ubuntu:kinetic
user: root
location: /
command_prefix: |
  # Make Bash not silently ignore errors.
  set -euo pipefail

tasks:
  update:
    command: |
      apt-get update

  install-debootstrap:
    dependencies: 
      - update
    command: |
      apt-get install debootstrap -y

  create-debootstrap:
    extra_docker_arguments: 
      - "--privileged" # Needed to run the command `unshare`
    cache: false
    dependencies:
      - install-debootstrap
    command: |
      debootstrap --variant=minbase kinetic /better-root
      unshare -muinpU -fr chroot better-root/ bash
```

Create a new chroot
```sh
debootstrap --variant=minbase kinetic /better-root
chroot better-root/ bash
```

Now we unshare some part of the system to close them off from other root-spaces.
```sh
unshare -muinpU -fr chroot better-root/ bash
```

Details
```sh
Options:
 -m, --mount[=<file>]      unshare mounts namespace
 -u, --uts[=<file>]        unshare UTS namespace (hostname etc)
 -i, --ipc[=<file>]        unshare System V IPC namespace
 -n, --net[=<file>]        unshare network namespace
 -p, --pid[=<file>]        unshare pid namespace
 -U, --user[=<file>]       unshare user namespace

 -f, --fork                fork before launching <program>
 -r, --map-root-user       map current user to root (implies --user)
```

## CGroup

Creating a config file `.toastrc.yml`
```yaml
# docker_cli: podman # Make sure to start podman before.
write_remote_cache: false # Whether Toast should write to remote cache, not useful in local dev.
```

Now the call to `toast` becomes
```shell
toast -s -c .toastrc.yml
```

Installing htop and cgroup-tools

in the `toast` file, add the task
```yaml
  install-cgoups:
    cache: false
    extra_docker_arguments: 
      - "--privileged" # Needed to run the command `unshare`.
    dependencies:
      - create-debootstrap
    command: |
      unshare -muinpU -fr chroot better-root/ bash
      apt-get install -y cgroup-tools htop
```

## Image

Run a image 
```sh
docker run -it alpine
```

Confirm you're in the right container by running
```sh
/ $ cat /etc/issue
Welcome to Alpine Linux 3.16
Kernel \r on an \m (\l)

/ $ 
```

## Pruning

You can end up accumulating a lot of images. To clean up, 
```sh
docker image prune
```

## Detach mode

You can run images on the background with the `--detach` flag.
It returns the hash of the image

```sh
% docker run -it --detach alpine 
b5a8d4584f5e9826b6fa036c7082a8ee8b36982ebb5ea7f311db20e2983ea307
% docker ps
CONTAINER ID   IMAGE     COMMAND     CREATED         STATUS         PORTS     NAMES
b5a8d4584f5e   alpine    "/bin/sh"   3 seconds ago   Up 3 seconds             elastic_tesla
```

and to attach to it
```sh
docker attach b5a8d4584f5e9826b6fa036c7082a8ee8b36982ebb5ea7f311db20e2983ea307
#or 
docker attach b5a8d4584f5e
# or 
docker attach elastic_tesla
```

## Image name
You can reference the image by specifying a name
```sh
docker run -it --name docker_fem alpine
% docker ps
CONTAINER ID   IMAGE     COMMAND     CREATED          STATUS          PORTS     NAMES
019246dfc1f2   alpine    "/bin/sh"   38 seconds ago   Up 38 seconds             docker_fem
```


# Node Image

Pull an image via
```sh
docker run -it node
```

Whoever build the image decides what command runs once the image has been pulled.
You can decide what to run instead. E.g.
```sh
docker run -it node bash
```

This will open a shell from inside the image. To fire `node` type in the command
```sh
% node
```

This will trigger the REPL.

# Using toast

With a `toast_node.yml` toast file ready, run
```sh
toast -c .toastrc.yml -f toast_node.yml -s
```

This will open a shell, and here again, call `node` to open the REPL.