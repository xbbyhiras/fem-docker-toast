import hapi from '@hapi/hapi'
import pino from 'hapi-pino'
import { MongoClient } from 'mongodb'

const url = process.env.MONGO_CONNECTION_STRING || 'mongodb://localhost:27017'
const dbName = 'dockerApp'
const collectionName = 'count'

const start = async () => {
  const client = await MongoClient.connect(url)
  const db = client.db(dbName)
  const collection = db.collection(collectionName)

  const server = hapi.server({
    host: '0.0.0.0', // host localhost will keep you in the container.
    port: process.env.PORT | 3000
  })

  server.route({
    method: 'GET',
    path: '/',
    handler: async () => {
      const count = await collection.estimatedDocumentCount()
      return { succes: true, count, update: 8 }
    }
  })
  
  server.route({
    method: 'GET',
    path: '/add',
    handler: async () => {
      const res = await collection.insertOne({})
      return { inserted: res.insertedId }
    }
  })

  // https://github.com/pinojs/hapi-pino/issues/147
  await server.register({
    plugin: pino,
    options: {
      redact: ['req.headers.authorization'],
      transport: {
        target: 'pino-pretty',
        options: {
          colorize: true,
          minimumLevel: 'info',
          levelFirst: true,
          messageFormat: true,
          timestampKey: 'time',
          translateTime: true,
          singleLine: false,
          mkdir: true,
          append: true
        }
      }
    }
  })

  await server.start()

  return server
}

start()
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
