# Dockerfile layers

Each instruction in the `Dockerfile` returns a new containers.
When a change occurs in the codebase, Docker checks at what intruction the change is being impactful and restart the build process form there.

## Non optimised example

```dockerfile
FROM node
USER node
WORKDIR /usr/node/hapi-server
COPY --chown=node:node . .
RUN npm ci
CMD [ "node", "index.mjs" ]
```

If a file is being modified, the instruction being impacted is the `COPY`.
The build passes the first 3 instructions and then restart copying files and redo the full install.

You don't need to redownload all the packages if you modify only the port number of the server.
To optimise this step, you need to create instructions that will only triggers a rebuild if `package.json` is modified.

The optimised version becomes:
```dockerfile
FROM node
USER node
WORKDIR /usr/node/hapi-server
COPY --chown=node:node package.json package-lock.json ./
RUN npm ci
COPY --chown=node:node . .
CMD [ "node", "index.mjs" ]
```